#!/bin/sh
sudo yum update -y
sudo yum install -y libreswan

sudo cp ipsec.conf /etc/
sudo cp nano-ipsec.conf /etc/ipsec.d/
sudo cp nano-ipsec.secrets /etc/ipsec.d/

sudo cp sysctl.conf /etc/sysctl.conf
sudo sysctl -p

sudo systemctl restart ipsec.service

#sudo ip addr add 3.123.107.18/32 dev lo:elastic
#sudo ip addr del 3.123.107.18/32 dev lo:elastic
#sudo ip link set ip_vti0 up
#sudo ip link set ip_vti0 down
#sudo ip route add 172.30.0.0/24 nexthop dev ip_vti0
