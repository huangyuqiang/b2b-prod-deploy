# Setup site-to-site IPsec VPN between IBS-B2B and BAS-NANO

## Steps
1. launch an EC2 instance (internal IP: 10.0.0.100) on selected VPC
1. associate EIP (3.123.107.18) 
1. disable Source/Dest. check 
1. ensure Security Group to enable 4500,500 port accessible from BAS
1. config route table to forward 172.30.0.0/24 to the instance
1. upload config file to EC2 instance
1. execute deploy.sh to deploy
1. execute status.sh to check status

## References
1. [Site-to-site Vpn Setup on Aws](http://sahilsk.github.io/articles/site-to-site-vpn-setup-on-aws/)
2. https://docs.cloud.oracle.com/iaas/Content/Network/Concepts/libreswan.htm
3. [libreswan](https://libreswan.org)
4. https://blog.csdn.net/gaoxiang2005/article/details/69135107
